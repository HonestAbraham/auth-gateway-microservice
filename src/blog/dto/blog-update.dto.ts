import { ApiProperty } from "@nestjs/swagger";

export class BlogUpdateDto {
    @ApiProperty({
        example : "New Example Title"
    })
    title: string;
    @ApiProperty({
        example : "New Example Content"
    })
    content: string;
}


