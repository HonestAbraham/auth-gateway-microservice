import { HttpException, HttpStatus, Inject, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { BlogRequestDto } from './dto/blog-request.dto';
import { BlogRepository } from './blog.repository';
import { ClientProxy } from '@nestjs/microservices';
import { BlogUpdateDto } from './dto/blog-update.dto';

@Injectable()
export class BlogService {



  constructor(
    private blogsRepository: BlogRepository,
    @Inject('BLOG') private readonly blogClient: ClientProxy,
  ) { }

  async createv2(userId: number, blogRequestDto: BlogRequestDto) {

    try {
      const fs = require('fs')
      let path = "./images_dump/" + blogRequestDto.imageId;

      // console.log(path)

      if (!fs.existsSync(path)) {
        throw new NotFoundException("Folder does not exist")
      }
      const files = fs.readdirSync(path);
      if (files.length === 0) {
        throw new NotFoundException()
      }
      const filename = files[0];
      const imageFilePath = path + '/' + filename

      if (!fs.existsSync(path)) {
        console.log(imageFilePath)
        throw new NotFoundException("test Image does not exist")
      }

      const blog = await this.blogsRepository.create(userId, blogRequestDto.title, blogRequestDto.content, blogRequestDto.blogType, blogRequestDto.imageId)

      let new_path = './images_persistent/' + blogRequestDto.imageId;

      fs.rename(path, new_path, function (err) {
        if (err) throw err
      })
      return blog;
    } catch (error) {
      throw new HttpException("Error", HttpStatus.BAD_REQUEST)
    }

  }

  getAllBlogsByUser(userId: number, skip: number, limit: number) {
    return this.blogClient.send<any>({ cmd: 'get_all_blogs_by_id' }, { userId, skip, limit })

  }

  findAll(skip: number, limit: number) {
    console.log("test")
    return this.blogClient.send<any>({ cmd: 'get_all_blogs' }, { skip, limit })
  }

  async update(isAdmin: boolean, id: number, userId: number, blogUpdateDto: BlogUpdateDto) {
    return this.blogClient.send<any>({ cmd: 'update_blog' }, { isAdmin, id, userId, blogUpdateDto })
  }

  remove(isAdmin: boolean, id: number, userId: number) {
    return this.blogClient.send<any>({ cmd: 'remove_blog' }, { isAdmin, id, userId })
  }

  getPersistentPath(hash: string): string {
    const fs = require('fs')
    let path = `./images_persistent/${hash}`;
    if (!fs.existsSync(path)) {
      console.log(path + "does not exist")
      throw new NotFoundException("Image does not exist")
    }

    const imageFolderPath = `./images_persistent/${hash}`;
    const files = fs.readdirSync(imageFolderPath);
    if (files.length === 0) {
      // Folder is empty, handle error or return appropriate response
      throw new NotFoundException()
    }
    const filename = files[0]; // Get the name of the file inside the folder
    const imageFilePath = imageFolderPath + '/' + filename
    return imageFilePath
  }


  getDumpPath(hash: string): string {
    const fs = require('fs')
    let path = `./images_dump/${hash}`;

    if (!fs.existsSync(path)) {
      throw new NotFoundException("Image does not exist")
    }

    const imageFolderPath = `./images_dump/${hash}`;
    const files = fs.readdirSync(imageFolderPath);
    if (files.length === 0) {

      throw new NotFoundException()
    }
    const filename = files[0];
    const imageFilePath = imageFolderPath + '/' + filename
    return imageFilePath
  }

}
