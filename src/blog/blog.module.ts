
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { Blog } from 'src/entities/blog.entity';
import { User } from 'src/entities/User';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { MulterModule } from '@nestjs/platform-express';
import { BlogController } from './blog.controller';
import { BlogRepository } from './blog.repository';
import { BlogService } from './blog.service';

@Module({
  imports: [TypeOrmModule.forFeature([User, Blog]), ConfigModule.forRoot({}),
  ClientsModule.register([
    {
      name: 'BLOG',
      transport: Transport.TCP,
      options: { port: 3007 },
    },
  ]),
  MulterModule.register({
    dest: './images_dump',
  }),],
  controllers: [BlogController],
  providers: [BlogService, BlogRepository]
})
export class BlogModule { }
