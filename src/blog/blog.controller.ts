import { BadRequestException, Body, Controller, Delete, Get, HttpStatus, Param, ParseIntPipe, Patch, Post, Query, Req, Res, UploadedFile, UseGuards, UseInterceptors } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiBody, ApiConsumes } from '@nestjs/swagger';
import { BlogRequestDto } from './dto/blog-request.dto';
import { makeResponse, makeResponseWithoutData } from 'src/util/output_wrapper';
import { FileInterceptor } from '@nestjs/platform-express';
import { generateFolderHash } from 'src/util/generateFolderHash';
import * as fs from 'fs';
import { basename, join } from 'path';
import { diskStorage } from 'multer';
import { PaginationBlogOptionDto } from './dto/PaginationBlogOption.dto';
import { BlogService } from './blog.service';
import { UserRole } from 'src/util/UserRole';
import { BlogUpdateDto } from './dto/blog-update.dto';
import { of } from 'rxjs';



@Controller('blog')
export class BlogController {

    constructor(private readonly blogService: BlogService) { }

    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @Post()
    async createV2(@Req() req, @Body() blogRequestDto: BlogRequestDto) {
        console.log("test v2")
        const userId = req.user.id;
        if (!blogRequestDto.title || !blogRequestDto.content || !blogRequestDto.imageId) {
            throw new BadRequestException('Missing required properties');
        }
        const createdBlog = await this.blogService.createv2(userId, blogRequestDto);
        return makeResponse(HttpStatus.OK, "Successsfully created Blog", createdBlog);
    }


    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @Get('me')
    async getAllBlogsByMe(@Req() req, @Query() paginationBlogOptionDto: PaginationBlogOptionDto) {
        const limit = paginationBlogOptionDto.limit;
        const page = paginationBlogOptionDto.page;
        if (limit < 1 || page < 1) {
            throw new BadRequestException('Invalid pagination parameters');
        }
        const skip = (page - 1) * limit;
        const userId = req.user.id;
        const myBlogs = await this.blogService.getAllBlogsByUser(userId, skip, limit);
        return makeResponse(HttpStatus.OK, "Successfully Get 'My' Blogs", myBlogs);
    }


    @Get()
    async findAll(@Query() paginationBlogOptionDto: PaginationBlogOptionDto) {
        const limit = paginationBlogOptionDto.limit;
        const page = paginationBlogOptionDto.page;
        if (limit < 1 || page < 1) {
            throw new BadRequestException('Invalid pagination parameters');
        }
        const skip = (page - 1) * limit;
        const allBlogs = await this.blogService.findAll(skip, limit);
        return makeResponse(HttpStatus.OK, "Successfully Get All Blogs", allBlogs);
    }



    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @Patch(':id(\\d+)')
    async update(@Req() req, @Param('id', ParseIntPipe) id: number, @Body() blogRequestDto: BlogUpdateDto) {
        const userId = req.user.id;

        const isAdmin = req.user.role == UserRole.Admin;

        const updatedBlog = await this.blogService.update(isAdmin, +id, userId, blogRequestDto);
        return makeResponse(HttpStatus.OK, "Successfully Update Blog", updatedBlog);
    }

    @ApiBearerAuth()
    @UseGuards(AuthGuard('jwt'))
    @Delete(':id(\\d+)')
    async remove(@Req() req, @Param('id', ParseIntPipe) id: number) {
        const userId = req.user.id;

        const isAdmin = req.user.role == UserRole.Admin;
        await this.blogService.remove(isAdmin, +id, userId);
        return makeResponseWithoutData(HttpStatus.OK, "Deleted blog");
    }


    @Post('upload')
    @ApiConsumes('multipart/form-data')
    @ApiBody({
        schema: {
            type: 'object',
            properties: {
                file: {
                    type: 'string',
                    format: 'binary',
                },
            },
        },
    })
    @UseInterceptors(FileInterceptor('file', {
        storage: diskStorage({
            destination: (req, file, cb) => {
                const folderHash = generateFolderHash(file.originalname);
                const folderPath = `./images_dump/${folderHash}`;
                fs.mkdirSync(folderPath, { recursive: true }); // Create the destination folder if it doesn't exist

                cb(null, folderPath);
            },
            filename: (req, file, cb) => {
                cb(null, file.originalname); // Use the original filename
            },
        }),
    }))

    uploadFile2(@UploadedFile() file) {
        // console.log(file)
        return makeResponse(HttpStatus.OK, "Successfully Uploaded file", basename(file.destination));
    }


    @Get('image/:hash')
    getImage(@Param('hash') hash: string, @Res() res) {
        let path: string = this.blogService.getPersistentPath(hash)
        console.log(path)
        return makeResponse(HttpStatus.OK, "Image in Persistent Folder", of(res.sendFile(join(process.cwd(), path))))

    }
}
// function diskStorage(arg0: { destination: (req: any, file: any, cb: any) => void; filename: (req: any, file: any, cb: any) => void; }): any {
//     throw new Error('Function not implemented.');
// }

