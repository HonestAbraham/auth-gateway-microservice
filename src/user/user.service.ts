import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { UserOptionDto } from './dto/UserOption.dto';
// import { CreateUserEvent } from 'src/util/create-user.event';
import { UpdateUserDto } from './dto/UpdateUser.dto';
import { firstValueFrom } from 'rxjs';
import { UserRole } from 'src/util/UserRole';

@Injectable()
export class UserService {

    constructor(
        @Inject('USER') private readonly userClient: ClientProxy,
    ) { }

    async onApplicationBootstrap() {
        await this.userClient.connect();
    }

    async getUser(userOptionsDto: UserOptionDto, req) {
        // console.log(req.user.id)
        // console.log(req)
        console.log("test")
        let response;

        if (!req["user"]) {
            // console.log("goes in here ya")
            response = this.userClient.send<any>({ cmd: 'get_limited_users' }, { ...userOptionsDto })
        } else if (req.user.role == UserRole.Member) {
            response = this.userClient.send<any>({ cmd: 'get_limited_users' }, { ...userOptionsDto })
        } else {
            response = this.userClient.send<any>({ cmd: 'get_all_users' }, { ...userOptionsDto })
        }
        // console.log(response)
        console.log(await firstValueFrom(response));
        
        return response;

    }

    findById(id: number) {
        return this.userClient.send<any>({ cmd: 'find_by_id' }, id)
    }

    findByUsername(username: string) {
        return this.userClient.send<any>({ cmd: 'find_by_username' }, username)
    }

    async update_my_user(req, updateUserDto: UpdateUserDto) {
        // console.log(req.user.id)
        const id = req.user.id;
        console.log("gateway updateUser - Service")
        this.userClient.emit(
            'update_this_user', { updateUserDto, id }
        )
    }

    async delete_my_user(req) {
        const id = req.user.id;
        console.log("gateway  deleteUser - Service")
        this.userClient.emit(
            'delete_this_user', { id: id }
        )
    }


    async update_other_user(id, updateUserDto: UpdateUserDto) {
        this.userClient.emit(
            'update_this_user', { updateUserDto, id }
        )
    }

    async delete_other_user(id) {
        this.userClient.emit('delete_this_user', { id })
    }
}
