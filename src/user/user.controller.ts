import { Body, Controller, Delete, ForbiddenException, Get, HttpStatus, Param, ParseIntPipe, Put, Query, Req, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { UserOptionDto } from 'src/user/dto/UserOption.dto';
import { UserService } from './user.service';
import { MessagePattern } from '@nestjs/microservices';
import { AuthGuard } from '@nestjs/passport';
import { UpdateUserDto } from './dto/UpdateUser.dto';
import { UserRole } from 'src/util/UserRole';
import { makeResponseWithoutData } from 'src/util/output_wrapper';

@Controller('user')
export class UserController {

    constructor(private readonly userService: UserService) { }


    @ApiTags("User")
    @ApiBearerAuth() @UseGuards(AuthGuard('jwt'))
    @Get()
    getUsers(
        @Query() userOptionDto: UserOptionDto, @Req() req
    ) {
        return this.userService.getUser(userOptionDto, req)

    }


    @ApiTags("User")
    @ApiBearerAuth() @UseGuards(AuthGuard('jwt'))
    @Get(':id(\\d+)')
    async getUserById(@Req() req, @Param('id', ParseIntPipe) userId: number) {
        return this.userService.findById(userId)
    }

    @ApiTags("User")
    @ApiBearerAuth() @UseGuards(AuthGuard('jwt'))
    @Get(':username')
    async findByUsername(@Param('username') username: string) {
        const regex = /^[a-zA-Z0-9]*[a-zA-Z][a-zA-Z0-9]*$/;
        if (!regex.test(username)) {
            throw new Error('Invalid username');
        }
    }



    @ApiTags("User")
    @ApiBearerAuth() @UseGuards(AuthGuard('jwt'))
    @Put("/me")
    updateThisUser(@Req() req, @Body() updateUserDto: UpdateUserDto) {
        this.userService.update_my_user(req, updateUserDto)
        return makeResponseWithoutData(HttpStatus.OK, "Successfully Updated User");

    }


    @ApiTags("User")
    @ApiBearerAuth() @UseGuards(AuthGuard('jwt'))
    @Delete("/me")
    async deleteThisUser(@Req() req) {
        await this.userService.delete_my_user(req);
        return makeResponseWithoutData(HttpStatus.OK, "Successfully deleted User");

    }


    @ApiTags("User")
    @ApiBearerAuth() @UseGuards(AuthGuard('jwt'))
    @Put(':id(\\d+)')
    async updateUserById(@Req() req, @Param('id', ParseIntPipe) id: number, @Body() updateUserDto: UpdateUserDto) {
        console.log(req)
        if (req.user.role == UserRole.Admin) {
            this.userService.update_other_user(id, updateUserDto)
        }
        else if (req.user.id != id) {
            throw new ForbiddenException("You do not have permission to update another User")
        }
    }



    @ApiTags("User")
    @ApiBearerAuth() @UseGuards(AuthGuard('jwt'))
    @Delete(':id(\\d+)')
    async deleteUserById(@Req() req, @Param('id', ParseIntPipe) id: number) {
        if (req.user.role == UserRole.Admin) {
            this.userService.delete_other_user(id)
        } else if (req.user.id != id) {
            throw new ForbiddenException("You do not have permission to delete another User")
        }
    }



}
