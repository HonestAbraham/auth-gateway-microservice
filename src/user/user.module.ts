import { Module } from '@nestjs/common';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { Blog } from 'src/entities/blog.entity';
import { User } from 'src/entities/User';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [TypeOrmModule.forFeature([User, Blog]), ConfigModule.forRoot({}),
  ClientsModule.register([
    {
      name: 'USER',
      transport: Transport.TCP,
      options: { port: 3006 },
    },
  ]),],
  controllers: [UserController],
  providers: [UserService]
})
export class UserModule {}
