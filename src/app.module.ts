import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';

import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { MulterModule } from '@nestjs/platform-express';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { ScheduleModule } from '@nestjs/schedule';
import { User } from './entities/User';
import { Blog } from './entities/blog.entity';
import { RequestLoggingMiddleware } from './util/logger.middleware';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { env } from 'process';
import { JwtStrategy } from './strategy/jwt.strategy';
import { JwtModule } from '@nestjs/jwt';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { BlogModule } from './blog/blog.module';


@Module({
  imports: [
  //   ClientsModule.register([
  //   {
  //     name: 'USER',
  //     transport: Transport.TCP,
  //     options: { port: 3006 },
  //   },
  //   {
  //     name: 'BLOG',
  //     transport: Transport.TCP,
  //     options: { port: 3007 },
  //   },
  // ]),
  ConfigModule.forRoot(), JwtModule.register({}), TypeOrmModule.forFeature([User, Blog]),


  TypeOrmModule.forRootAsync({
    imports: [ConfigModule],
    useFactory: (configService: ConfigService) => ({
      type: 'mysql',
      host: configService.get<string>('DATABASE_HOST'),
      port: parseInt(configService.get<string>('DATABASE_PORT')),
      username: configService.get<string>('DATABASE_USER'),
      password: configService.get<string>('DATABASE_PASS'),
      database: configService.get<string>('DATABASE_NAME'),
      entities: [User, Blog],
      synchronize: false,
      logging: ["error", "query"]
    }),
    inject: [ConfigService],
  }),

    UserModule,

    AuthModule,

    BlogModule],
  controllers: [AppController],
  providers: [AppService, JwtStrategy],
})
export class AppModule /*implements NestModule*/ {

}