import { ForbiddenException, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/User';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { CreateUserDto } from './auth/dto/CreateUser.dto';
import * as bcrypt from 'bcrypt';
import { UserLoginDto } from './auth/dto/UserLogin.dto';


@Injectable()
export class AppService {
   
}
