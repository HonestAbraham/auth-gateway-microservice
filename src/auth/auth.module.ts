import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/entities/User';
import { Blog } from 'src/entities/blog.entity';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [TypeOrmModule.forFeature([User, Blog]), JwtModule.register({}), ConfigModule.forRoot({})],

  controllers: [AuthController],
  providers: [AuthService]
})
export class AuthModule { }
