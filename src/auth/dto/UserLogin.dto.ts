
import { ApiProperty } from "@nestjs/swagger";

export class UserLoginDto {
    @ApiProperty({
        description: "User Email or Username",
        example: "jdoe.test@gmail.com"
    })
    loginVal: string;
    
    @ApiProperty({
        description: "User Password",
        example: "johnDoePassword123"
    })
    password: string; 
}

