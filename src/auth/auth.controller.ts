import { Body, Controller, Post } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateUserDto } from 'src/auth/dto/CreateUser.dto';
import { AuthService } from './auth.service';
import { UserLoginDto } from './dto/UserLogin.dto';

@Controller('auth')
export class AuthController {

    constructor(private readonly authService: AuthService){

    }

    @ApiTags("Auth")
    @Post('signup')
    createUser(@Body() createUserDto: CreateUserDto) {
        const token = this.authService.createUser(createUserDto);
        return token;
    }


    @ApiTags("Auth")
    @Post('login')
    userLogin(@Body() loginDto: UserLoginDto) {
        const token = this.authService.userLogin(loginDto);
        return token;
    }
}
