import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ApiBearerAuth } from '@nestjs/swagger';
import { InjectRepository } from '@nestjs/typeorm';
import { ExtractJwt, Strategy, } from 'passport-jwt';
import { User } from 'src/entities/User';
import { Repository } from 'typeorm';

@Injectable()
@ApiBearerAuth()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt',) {
    constructor(config: ConfigService, @InjectRepository(User) private userRepo: Repository<User>) {
        super({
            jwtFromRequest:
                ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: config.get('JWT_SECRET'),
        });
    }

    async validate(payload: { sub: number; email: string; }) {
        const user = await this.userRepo.findOneBy({ id: payload.sub });

        return user;
    }
}