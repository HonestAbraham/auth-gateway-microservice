import * as crypto from 'crypto';

export function generateFolderHash(originalName: string): string {
  const currentDate = new Date().toISOString().slice(0, 10);
  const currentTime = new Date().getTime();
  const hash = crypto.createHash('md5').update(currentDate + currentTime + originalName).digest('hex');
  return hash;
}
