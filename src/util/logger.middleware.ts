import { Injectable, NestMiddleware, Logger } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';


@Injectable()
export class RequestLoggingMiddleware implements NestMiddleware {
    private readonly logger = new Logger(RequestLoggingMiddleware.name);

    use(req: Request, res: Response, next: () => void) {

        const deepCopy = JSON.parse(JSON.stringify(req.body));

        if (deepCopy.hasOwnProperty("password")){
            delete deepCopy["password"]
        }

        const startTime = Date.now();

        res.on('finish', () => {
            const endTime = Date.now();
            const responseTime = endTime - startTime;

            this.logger.log(`Response time: ${responseTime}ms`);
            // console.log(res)
        });

        this.logger.log(
            `Received ${req.method} request to ${req.originalUrl} with Query Parameter: ${req.query} from from IP: ${req.ip} ${req.headers['user-agent']}`,
            JSON.stringify({
                body: deepCopy,
                query: req.query,
            }),
        );
        next();
    }
}
