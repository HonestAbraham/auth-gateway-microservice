import { Body, Controller, Get, Post, Query, Req } from '@nestjs/common';
import { AppService } from './app.service';
import { ApiTags } from '@nestjs/swagger';
import { CreateUserDto } from './auth/dto/CreateUser.dto';
import { UserLoginDto } from './auth/dto/UserLogin.dto';
import { UserOptionDto } from './user/dto/UserOption.dto';

@Controller()
export class AppController {
}
